/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */
import 'react-native-gesture-handler';
import React from 'react';
import {AuthUserProvider} from './src/context/AuthUserProvider';
import {ThemeProvider} from './src/context/ThemeProvider';
import {AppRouter} from './src/stack/AppRouter';

const App = () => {
  return (
    <ThemeProvider>
      <AuthUserProvider>
        <AppRouter />
      </AuthUserProvider>
    </ThemeProvider>
  );
};

export default App;
