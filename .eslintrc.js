module.exports = {
  env: {
    'react-native/react-native': true,
  },
  root: true,
  extends: '@react-native-community',
  rules: {
    'no-unused-vars': 2,
    'no-undef': 0,
    'react-native/no-unused-styles': 2,
    'react-native/no-raw-text': 2,
    'react-native/no-inline-styles': 2,
    'react-native/no-single-element-style-arrays': 2,
    'promise/prefer-await-to-then': 0,
    'promise/catch-or-return': 0,
  },
};
