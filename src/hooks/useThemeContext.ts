import {useCallback, useContext} from 'react';
import {ThemeContext, ThemeData, defaultTheme} from '../context/ThemeProvider';

export const blackTheme: ThemeData = {
  buttonColor: 'gray',
  primaryColor: 'black',
  textColor: '#fff',
};

export default function useThemeContext() {
  const themeContext = useContext(ThemeContext);
  const currentTheme = themeContext?.theme;
  const setDefaultTheme = useCallback(
    () => themeContext?.setTheme?.(defaultTheme),
    [themeContext],
  );
  const setBlackTheme = useCallback(
    () => themeContext?.setTheme?.(blackTheme),
    [themeContext],
  );
  return {themeContext, currentTheme, setDefaultTheme, setBlackTheme};
}
