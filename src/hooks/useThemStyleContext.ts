import {useMemo} from 'react';
import {ViewStyle} from 'react-native';
import useThemeContext from './useThemeContext';

export default function useThemeStyleContext() {
  const {currentTheme} = useThemeContext();
  const backgroundStyle: ViewStyle = useMemo(
    () => ({
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: currentTheme?.primaryColor,
    }),
    [currentTheme],
  );
  const buttonBackgroundColor = useMemo(
    () => ({
      backgroundColor: currentTheme?.buttonColor,
    }),
    [currentTheme],
  );
  const textColor = useMemo(
    () => ({
      color: currentTheme?.textColor,
    }),
    [currentTheme],
  );
  return {backgroundStyle, buttonBackgroundColor, textColor};
}
