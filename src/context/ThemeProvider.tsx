import React, {PropsWithChildren, useMemo, useState} from 'react';

export type ThemeData = {
  textColor: string;
  buttonColor: string;
  primaryColor: string;
};

export const defaultTheme: ThemeData = {
  textColor: '#000',
  buttonColor: '#007AFF',
  primaryColor: '#fff',
};

type ThemeContextType = {
  theme: ThemeData;
  setTheme?: (theme: ThemeData) => void;
};

export const ThemeContext = React.createContext<ThemeContextType>({
  theme: defaultTheme,
});

export const ThemeProvider: React.FC<PropsWithChildren<React.ReactNode>> = ({
  children,
}) => {
  const [theme, setTheme] = useState<ThemeData>(defaultTheme);

  const themeValue = useMemo(() => ({theme, setTheme}), [theme, setTheme]);

  return (
    <ThemeContext.Provider value={themeValue}>{children}</ThemeContext.Provider>
  );
};
