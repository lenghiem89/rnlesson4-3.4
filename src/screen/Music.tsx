import React from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';
import {MainStackScreenProps} from '../stack/Navigation';

const MusicScreen: React.FC<MainStackScreenProps<'StoryScreen'>> = ({
  navigation,
  route,
}) => {
  return (
    <View style={styles.screen}>
      <Button
        title="trở về trang chủ"
        onPress={() => {
          navigation.navigate('Home');
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    textAlign: 'center',
    alignItems: 'center',
  },
});

export default MusicScreen;
