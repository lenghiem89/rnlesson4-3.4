import React from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';
import {MainStackScreenProps} from '../stack/Navigation';

const CategoryScreen: React.FC<MainStackScreenProps<'StoryScreen'>> = ({
  navigation,
  route,
}) => {
  return (
    <View style={styles.screen}>
      <Button
        title="StoryScreen"
        onPress={() => {
          navigation.navigate('StoryScreen');
        }}
      />
      <Button
        title="MusicScreen"
        onPress={() => {
          navigation.navigate('MusicScreen');
        }}
      />
      <Button
        title="Picture"
        onPress={() => {
          navigation.navigate('PictureScreen');
        }}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  screen: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});

export default CategoryScreen;
