import {useNavigation} from '@react-navigation/native';
import React, {useReducer} from 'react';
import {Text, Button, View, StyleSheet} from 'react-native';
import {MainStackNavigation} from '../../stack/Navigation';

export type CounterState = {
  clicks: number;
  prevValue: number[];
};

enum ActionType {
  RESET,
  INCREMENT,
  INCREMENT_BY_AMOUNT,
  DECREMENT,
  UNDO,
}
type Reset = {type: ActionType.RESET};
type Increment = {type: ActionType.INCREMENT};
type IncrementByAmount = {type: ActionType.INCREMENT_BY_AMOUNT; value: number};
type Decrement = {type: ActionType.DECREMENT};
type Undo = {type: ActionType.UNDO};
type Action = Reset | Increment | IncrementByAmount | Decrement | Undo;
const increment = (): Increment => ({
  type: ActionType.INCREMENT,
});
const incrementByAmount = (value: number): IncrementByAmount => ({
  type: ActionType.INCREMENT_BY_AMOUNT,
  value: value,
});
const decrement = (): Decrement => ({
  type: ActionType.DECREMENT,
});
const reset = (): Reset => ({
  type: ActionType.RESET,
});
const undo = (): Undo => ({
  type: ActionType.UNDO,
});

const reducer = (state: CounterState, action: Action) => {
  switch (action.type) {
    case ActionType.RESET:
      return {
        clicks: 0,
        prevValue: [],
      };
    case ActionType.INCREMENT:
      return {
        clicks: state.clicks + 1,
        prevValue: [...state.prevValue, state.clicks],
      };
    case ActionType.INCREMENT_BY_AMOUNT:
      return {
        clicks: state.clicks + action.value,
        prevValue: [...state.prevValue, state.clicks],
      };
    case ActionType.DECREMENT:
      return {
        clicks: state.clicks - 1,
        prevValue: [...state.prevValue, state.clicks],
      };
    case ActionType.UNDO:
      const pre = state.prevValue.pop();
      return {
        clicks: pre,
        prevValue: state.prevValue,
      };
    default:
      return state;
  }
};

export const Counter = () => {
  const [state, dispatch] = useReducer(reducer, {
    clicks: 0,
    prevValue: [],
  });

  const mainStackNavigation = useNavigation<MainStackNavigation>();

  return (
    <View style={styles.counter}>
      <Button
        onPress={() => {
          mainStackNavigation.push('Detail', {
            id: Math.floor(Math.random() * 100),
          });
        }}
        title="Use Hook Navigation"
      />
      <Text style={styles.text}>{state.clicks}</Text>
      <Button onPress={() => dispatch(increment())} title="Increment" />
      <Button
        onPress={() => dispatch(incrementByAmount(5))}
        title="Increment Amount"
      />
      <Button onPress={() => dispatch(decrement())} title="Decrement" />
      <Button onPress={() => dispatch(reset())} title="Reset" />
      <Button
        onPress={() => dispatch(undo())}
        disabled={state.prevValue.length === 0}
        title="Undo"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  counter: {
    borderRadius: 10,
    borderWidth: 1,
    borderStyle: 'dashed',
    borderColor: 'gray',
    padding: 20,
  },
  text: {
    color: 'white',
    backgroundColor: 'black',
    textAlign: 'center',
  },
});
