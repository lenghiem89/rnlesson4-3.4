import React, {useCallback, useEffect, useState} from 'react';

import {View, Text, StyleSheet, TextInput, Button, Alert} from 'react-native';
import MyTextInputRef, {
  MyTextInputHandle,
} from '../../component/MyTextInputRef';
import {useCounter} from './useCounter';

type DemoProps = {
  array: Array<number>;
};
const Demo: React.FC<DemoProps> = ({array}) => {
  console.log('Demo- render');
  return (
    <>
      {array.map(item => (
        <Text key={item}>{item}</Text>
      ))}
    </>
  );
};

const DemoMemo = React.memo(Demo);

type FourProps = {
  title?: string;
};
const Four: React.FC<FourProps> = ({title = 'My four component'}) => {
  const [name, setName] = useState<string>();

  const {count, incrementCount, array} = useCounter();

  const ref = React.useRef<MyTextInputHandle>(null); // assign null makes it compatible with elements.

  useEffect(() => {
    console.log('name changed to ', name);
  }, [name]);

  useEffect(() => {
    console.log(title);
  }, [title]);

  console.log('Four - render');

  const updateName = useCallback(() => {
    console.log('updateName click');
    setName('Name' + Math.random());
    ref?.current?.setTextValue('Name' + Math.random());
    return 0;
  }, []);

  const getNameRef = useCallback(() => {
    Alert.alert('Get Name', ref?.current?.getText() ?? '--');
  }, []);

  return (
    <View>
      <Text style={styles.second}>{title}</Text>
      <Text style={styles.second}>{count}</Text>
      <Text style={styles.second}>{name}</Text>
      <TextInput style={styles.textInput} onChangeText={setName} value={name} />
      <MyTextInputRef style={styles.textInput} ref={ref} />
      <Button color="green" title="Update Count" onPress={incrementCount} />
      <Button color="green" title="Update Name" onPress={updateName} />
      <Button color="red" title="Get Name Ref" onPress={getNameRef} />
      <DemoMemo array={array} />
    </View>
  );
};

const styles = StyleSheet.create({
  textInput: {
    height: 50,
    borderColor: 'gray',
    borderRadius: 10,
    marginHorizontal: 10,
    borderWidth: 1,
  },
  second: {
    fontSize: 20,
    color: 'gray',
  },
});

export default Four;
