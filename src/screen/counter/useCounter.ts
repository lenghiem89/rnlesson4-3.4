import {useCallback, useEffect, useMemo, useState} from 'react';

export const useCounter = () => {
  const [count, setCount] = useState(0);
  const array = useMemo(() => {
    const arr = [];
    for (let i = 0; i < count; i++) {
      arr.push(i);
    }
    return arr;
  }, [count]);

  useEffect(() => {
    console.log('count changed to ', count);
  }, [count]);

  const incrementCount = useCallback(() => {
    console.log('updateCount click', count);
    setCount(pre => ++pre);
  }, [count]);

  return {count, incrementCount, array};
};
