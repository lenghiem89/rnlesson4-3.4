import React from 'react';
import {Button, View, Text} from 'react-native';
import ThemeButton from '../component/ThemeButton';
import useThemeContext from '../hooks/useThemeContext';
import useThemeStyleContext from '../hooks/useThemStyleContext';
import {MainStackScreenProps} from '../stack/Navigation';

const Home: React.FC<MainStackScreenProps<'Home'>> = ({navigation, route}) => {
  const theme = useThemeStyleContext();
  const {setDefaultTheme, setBlackTheme} = useThemeContext();
  return (
    <View style={theme.backgroundStyle}>
      <Text style={theme.textColor}>Home Screen</Text>
      {route?.params && (
        <Text style={theme.textColor}>
          With received params : {route?.params?.userId}
        </Text>
      )}
      <Button
        title="Detail Screen"
        color={theme?.buttonBackgroundColor?.backgroundColor}
        onPress={() => {
          navigation.navigate('Detail', {
            id: Math.floor(Math.random() * 100),
          });
        }}
      />
      <Button
        title="Category Screen"
        color={theme?.buttonBackgroundColor?.backgroundColor}
        onPress={() => {
          navigation.navigate('CategoryScreen');
        }}
      />
      <ThemeButton buttonText="Change Black Theme" onPress={setBlackTheme} />
      <ThemeButton
        buttonText="Change Default Theme"
        onPress={setDefaultTheme}
      />
    </View>
  );
};
export default Home;
