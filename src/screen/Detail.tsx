import {useNavigation} from '@react-navigation/core';
import React, {useContext, useMemo} from 'react';
import {Text, View, ViewStyle} from 'react-native';
import MyButton, {styles} from '../component/MyButton';
import ThemeButton from '../component/ThemeButton';
import {ThemeContext} from '../context/ThemeProvider';
import useThemeContext from '../hooks/useThemeContext';
import {MainStackNavigation, MainStackScreenProps} from '../stack/Navigation';
import {Counter} from './counter/CounterReducerDemo';

export const Detail: React.FC<MainStackScreenProps<'Detail'>> = ({route}) => {
  const mainStackNavigation = useNavigation<MainStackNavigation>();
  const themeContext = useContext(ThemeContext);
  const {currentTheme} = useThemeContext();
  const backgroundStyle: ViewStyle = useMemo(
    () => ({
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: themeContext?.theme?.primaryColor,
    }),
    [themeContext],
  );
  const buttonBackgroundColor = useMemo(
    () => ({
      backgroundColor: currentTheme.buttonColor,
    }),
    [currentTheme],
  );

  const textColor = useMemo(
    () => ({
      color: currentTheme.textColor,
    }),
    [currentTheme],
  );

  return (
    <View style={backgroundStyle}>
      <Text style={textColor}>
        Details Screen with params: {route?.params?.id}
      </Text>
      <ThemeButton
        buttonText="Go other detail screen"
        onPress={() => {
          mainStackNavigation.push('Detail', {
            id: Math.floor(Math.random() * 100),
          });
        }}
      />
      <ThemeButton
        buttonText="Go Back Home screen with put back param"
        onPress={() => {
          mainStackNavigation.navigate({
            name: 'Home',
            params: {userId: Math.floor(Math.random() * 100)},
          });
        }}
      />
      <ThemeButton
        buttonText="Go Back Top screen"
        onPress={() => {
          mainStackNavigation.popToTop();
        }}
      />
      <MyButton
        style={[styles.button, buttonBackgroundColor]}
        textStyle={textColor}
        buttonText="Go Back"
        onPress={() => {
          mainStackNavigation.goBack();
        }}
      />
      <Counter />
    </View>
  );
};
