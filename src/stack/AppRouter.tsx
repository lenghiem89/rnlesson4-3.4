import {NavigationContainer} from '@react-navigation/native';
import React, {useContext} from 'react';
import {AuthUserContext} from '../context/AuthUserProvider';
import LoginStack from './LoginStack';
import {SelectStack} from './SelectStack';

export const AppRouter = () => {
  const auth = useContext(AuthUserContext);
  return (
    <NavigationContainer>
      {auth.isAuth ? <SelectStack /> : <LoginStack />}
    </NavigationContainer>
  );
};
