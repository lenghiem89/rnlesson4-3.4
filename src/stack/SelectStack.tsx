import React, {useState} from 'react';
import {Button} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Header} from 'react-native/Libraries/NewAppScreen';
import MainStack from './MainStack';

export type StackType = 'DrawerStack' | 'BottomBarStack';
export const SelectStack = () => {
  const [stack, setStack] = useState<StackType>();

  return !stack ? (
    <SafeAreaView>
      <Header />
      <Button title="Drawer" onPress={() => setStack('DrawerStack')} />
      <Button title="BottomBar" onPress={() => setStack('BottomBarStack')} />
    </SafeAreaView>
  ) : (
    <MainStack stackType={stack} />
  );
};
