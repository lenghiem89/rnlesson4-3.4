import React from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
} from '@react-navigation/drawer';
import {TabStackParamList} from './Navigation';
import {DRAWER_STACK_SCREEN} from './Screen';
import {Button} from 'react-native';

const Drawer = createDrawerNavigator<TabStackParamList>();
const DrawerStack = () => {
  return (
    <Drawer.Navigator
      drawerContent={props => (
        <DrawerContentScrollView {...props}>
          <Button
            title="Home"
            onPress={() => props.navigation.navigate('Home')}
          />

          <Button
            title="About"
            onPress={() => props.navigation.navigate('About')}
          />
        </DrawerContentScrollView>
      )}>
      {DRAWER_STACK_SCREEN.map(item => (
        <Drawer.Screen
          key={item.name}
          name={item.name as keyof TabStackParamList}
          component={item.component}
          options={item.options}
        />
      ))}
    </Drawer.Navigator>
  );
};

export default DrawerStack;
