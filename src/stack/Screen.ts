import {Detail} from '../screen/Detail';
import Home from '../screen/Home';
import About from '../screen/Setting';
import BottomTabStack from './BottomTabStack';
import DrawerStack from './DrawerStack';
import TabIcon from './TabIcon';
import CategoryScreen from '../screen/Category';
import StoryScreen from '../screen/Story';
import MusicScreen from '../screen/Music';
import PictureScreen from '../screen/Picture';

export type Screen = {
  name: string;
  component: any;
  options?: any;
};

export const MAIN_STACK_SCREEN: Screen[] = [
  {
    name: 'MainDrawer',
    component: DrawerStack,
  },
  {
    name: 'MainTab',
    component: BottomTabStack,
  },
  {
    name: 'Detail',
    component: Detail,
  },
];

export const BOTTOM_TAB_STACK_SCREEN: Screen[] = [
  {
    name: 'Home',
    component: Home,
    options: {
      tabBarIcon: ({color, size}) => {
        return TabIcon('home', color, size);
      },
    },
  },
  {
    name: 'About',
    component: About,
    options: {
      tabBarIcon: ({color, size}) => {
        return TabIcon('information', color, size);
      },
    },
  },
];

export const DRAWER_STACK_SCREEN: Screen[] = [
  {
    name: 'Home',
    component: Home,
  },
  {
    name: 'About',
    component: About,
  },
  {
    name: 'CategoryScreen',
    component: CategoryScreen,
  },
  {
    name: 'StoryScreen',
    component: StoryScreen,
  },
  {
    name: 'MusicScreen',
    component: MusicScreen,
  },
  {
    name: 'PictureScreen',
    component: PictureScreen,
  },
];
