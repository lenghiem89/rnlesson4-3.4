import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {
  DrawerStackParamList,
  MainStackParamList,
  TabStackParamList,
} from './Navigation';
import {MAIN_STACK_SCREEN} from './Screen';
import {StackType} from './SelectStack';
const Stack =
  createStackNavigator<
    DrawerStackParamList & TabStackParamList & MainStackParamList
  >();
type Props = {
  stackType: StackType;
};
const MainStack = ({stackType}: Props) => {
  return (
    <Stack.Navigator
      initialRouteName={stackType === 'DrawerStack' ? 'MainDrawer' : 'MainTab'}>
      {MAIN_STACK_SCREEN.map(item => (
        <Stack.Screen
          key={item.name}
          name={item.name as keyof MainStackParamList}
          component={item.component}
          options={item.options}
        />
      ))}
    </Stack.Navigator>
  );
};

export default MainStack;
